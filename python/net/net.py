# Definir los nodos
nodos = ["Galaxia a", "Galaxia b", "Galaxia c"]

# Inicializar el grafo como un diccionario vacío
grafo = {}

# Agregar los nodos al grafo
for nodo in nodos:
    grafo[nodo] = {"links":[]}
    
# Agregar conexiones entre los nodos (enlaces)
grafo["Galaxia a"]["links"].append({"galaxia":"Galaxia b", "Distancia":10})
grafo["Galaxia b"]["links"].append({"galaxia":"Galaxia a", "Distancia":5  })
grafo["Galaxia b"]["links"].append({"galaxia":"Galaxia c", "Distancia":2  })
grafo["Galaxia c"]["links"].append({"galaxia":"Galaxia b", "Distancia":10 })

print(grafo)
# Mostrar el grafo
for key,value in grafo.items():
    print(f"Nodo {key}: Conexiones -> ")
    for link in value["links"]:
        print(link["galaxia"])
