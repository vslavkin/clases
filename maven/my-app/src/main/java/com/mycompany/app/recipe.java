import java.util.ArrayList;
import java.util.List;

class Recipe {
    private String name;
    private List<String> ingredients;
    private List<String> instructions;
    private double rating;

    public Recipe(String name) {
	this.name = name;
	this.ingredients = new ArrayList<>();
	this.instructions = new ArrayList<>();
	this.rating = 0;
    }

    // Getters and setters

    public void addIngredient(String ingredient) {
	ingredients.add(ingredient);
    }

    public void addInstruction(String instruction) {
	instructions.add(instruction);
    }

    public void setRating(double rating) {
	this.rating = rating;
    }

    public String getName() {
	return this.name;
    }
    public List<String> getIngredients() {
	return this.ingredients;
    }
    public List<String> getInstructions() {
	return this.instructions;
    }
    public double getRating() {
	return this.rating;
    }

}

class Review {
    private Recipe recipe;
    private String review;
    private double rating;

    public Review(Recipe recipe, String review, double rating) {
	this.recipe = recipe;
	this.review = review;
	this.rating = rating;
    }

    // Getters and setters
    public void setRecipe(Recipe rec) {
	this.recipe = rec;
    }
    public void setReview(String str) {
	this.review = str;
    }
    public void setRating(double r) {
	this.rating = r;
    }

    public Recipe getRecipe() {
	return this.recipe;
    }
    public String getReview() {
	return this.review;
    }
    public double getRating() {
	return this.rating;
    }

}
