#+title: Notas sobre python (y programacion en general)

* Argumentos
Los argumentos en las funciones pueden ser pasados "por valor" o "por referencia". Esto depende del tipo de dato que se utilice en el argumento, del lenguaje, etc.
En python los int, floats y strings son pasados por valor, lo que quiere decir, que si se modifica el valor del argumento dentro de la funcion, no se modificara al argumento en el nivel superior
#+begin_src python :results output
  def mas_uno(a):
            print("Dentro de la funcion")
            a[0] = a[0]+1
            print(a[0])

  def main():
            valor = [0]
            print("Antes de todo")
            print(valor[0])
            mas_uno(valor)
            print("Despues de la funcion")
            print(valor[0])

  if __name__=="__main__":
      main()
#+end_src

#+RESULTS:
: Antes de todo
: 0
: Dentro de la funcion
: 1
: Despues de la funcion
: 1

En cambio, los diccionarios, las listas y las tuplas se pasan por referencia:
#+begin_src python :results output
  def mas_uno(a):
            print("Dentro de la funcion")
            a[0] += 1
            print(a[0])

  def main():
            valor = [0,1,2,3]
            print("Antes de todo")
            print(valor[0])
            mas_uno(valor)
            print("Despues de la funcion")
            print(valor[0])

  if __name__=="__main__":
      main()
#+end_src

#+RESULTS:
: Antes de todo
: 0
: Dentro de la funcion
: 1
: Despues de la funcion
: 1

* Mostar imagenes
Para mostrar imagenes utilizamos la biblioteca PIL o Pillow
#+begin_src python
  # importing Image class from PIL package 
  from PIL import Image 

  # creating a object 
  im = Image.open(r"/home/vslavkin/Pictures/protector.png") 

  im.show()
#+end_src

Abre una ventana en tu navegador.
Si no queremos utilizar el navegador:
#+begin_src python
  # importing Image class from PIL package 
  from PIL import ImageTk, Image 
  import tkinter as tk

  # creating a object 
  im = Image.open(r"/home/vslavkin/Pictures/protector.png") 
  window = tk.Tk()
  tkimage = ImageTk.PhotoImage(im)
  label = tk.Label(image=tkimage)
  label.pack()
  window.mainloop()
#+end_src

* Leer desde un archivo
** Dot
Para leer desde un archivo todos los nodos podemos utilizar (en este caso particular) la funcion read_dot de networkx, que lee un archivo .dot y devuelve un =MultiGraph= o un =MultiDiGraph=

#+begin_src python
  import networkx as nx
  from networkx.drawing.nx_pydot import read_dot

  # Specify the path to your DOT file
  dot_file_path = "k5.dot"

  # Read the DOT file and create a graph
  G = read_dot(dot_file_path)

  # Now, you can access the edge attributes and add them as weights to the edges
  for u, v, data in G.edges(data=True):
      weight = data.get('weight', 1)  # Default weight is 1 if 'weight' attribute is not found
      G[u][v]['weight'] = weight

  # You can access the edge weights like this:
  print("Edge Weights:")
  for u, v, data in G.edges(data=True):
      print(f"{u} -- {v}: Weight = {data['weight']}")
      
#+end_src

#+begin_src dot :tangle k5.dot
graph G {
    A -- B [weight=2];
    B -- C;
    C -- A [weight=3];
}
#+end_src
** JSON
#+begin_src python :output stdio
import json
diccionario = {'hola':1, 'chau':2}
print(json.dumps(diccionario))
#+end_src

#+RESULTS:
: None
