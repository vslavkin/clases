public interface Grafo<T> {
    void agregarVertice(Vertice<T> v);
    void eliminarVertice(Vertice<T> v);
    void conectar(Vertice<T> origen, Vertice<T> destino);
    void conectar(Vertice<T> origen, Vertice<T> destino, int peso);
    void desConectar(Vertice<T> origen, Vertice<T> destino);
    boolean esAdyacente(Vertice<T> origen, Vertice<T> destino);
    boolean esVacio();
    ListaGenerica<Vertice<T>> listaDeVertices();
    int peso(Vertice<T> origen, Vertice<T> destino);
    ListaGenerica<Arista<T>> listaDeAdyacentes(Vertice<T> v);
    Vertice<T> vertice(int posicion);
}
