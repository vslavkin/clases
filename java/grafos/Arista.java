public interface Arista<T> {
    Vertice<T> verticeDestino();
    int peso();
}
