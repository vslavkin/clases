import java.util.*;


public class GrafoImpl<T> implements Grafo<T> {
    private Map<Vertice<T>, List<Arista<T>>> adjList;

    public GrafoImpl() {
	this.adjList = new HashMap<>();
    }

    @Override
    public void agregarVertice(Vertice<T> v) {
	if (!adjList.containsKey(v)) {
	    adjList.put(v, new LinkedList<>());
	}
    }

    @Override
    public void eliminarVertice(Vertice<T> v) {
	adjList.remove(v);
	for (List<Arista<T>> edges : adjList.values()) {
	    edges.removeIf(edge -> edge.verticeDestino().equals(v));
	}
    }

    @Override
    public void conectar(Vertice<T> origen, Vertice<T> destino) {
	conectar(origen, destino, 0);
    }

    @Override
    public void conectar(Vertice<T> origen, Vertice<T> destino, int peso) {
	if (adjList.containsKey(origen) && adjList.containsKey(destino)) {
	    adjList.get(origen).add(new AristaImpl<>(destino, peso));
	}
    }

    @Override
    public void desConectar(Vertice<T> origen, Vertice<T> destino) {
	if (adjList.containsKey(origen)) {
	    adjList.get(origen).removeIf(edge -> edge.verticeDestino().equals(destino));
	}
    }

    @Override
    public boolean esAdyacente(Vertice<T> origen, Vertice<T> destino) {
	if (adjList.containsKey(origen)) {
	    for (Arista<T> edge : adjList.get(origen)) {
		if (edge.verticeDestino().equals(destino)) {
		    return true;
		}
	    }
	}
	return false;
    }

    @Override
    public boolean esVacio() {
	return adjList.isEmpty();
    }

    @Override
    public ListaGenerica<Vertice<T>> listaDeVertices() {
	ListaGenerica<Vertice<T>> lista = new ListaGenerica<>();
	lista.addAll(adjList.keySet());
	return lista;
    }

    @Override
    public int peso(Vertice<T> origen, Vertice<T> destino) {
	if (adjList.containsKey(origen)) {
	    for (Arista<T> edge : adjList.get(origen)) {
		if (edge.verticeDestino().equals(destino)) {
		    return edge.peso();
		}
	    }
	}
	return 0;
    }

    @Override
    public ListaGenerica<Arista<T>> listaDeAdyacentes(Vertice<T> v) {
	ListaGenerica<Arista<T>> lista = new ListaGenerica<>();
	if (adjList.containsKey(v)) {
	    lista.addAll(adjList.get(v));
	}
	return lista;
    }

    @Override
    public Vertice<T> vertice(int posicion) {
	for (Vertice<T> v : adjList.keySet()) {
	    if (v.posicion() == posicion) {
		return v;
	    }
	}
	return null;
    }
}
