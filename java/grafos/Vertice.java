public interface Vertice<T> {
    T dato();
    void setDato(T d);
    int posicion();
}
