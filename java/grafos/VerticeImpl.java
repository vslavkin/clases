public class VerticeImpl<T> implements Vertice<T> {
    private T dato;
    private int posicion;

    public VerticeImpl(T dato, int posicion) {
	this.dato = dato;
	this.posicion = posicion;
    }

    @Override
    public T dato() {
	return dato;
    }

    @Override
    public void setDato(T dato) {
	this.dato = dato;
    }

    @Override
    public int posicion() {
	return this.posicion;
    }
}
