public class ciudades <T>{
    public ListaGenerica<Vertice<T>> dfsConCosto(Grafo<T> grafo, Vertice<T> start, Vertice<T> end) {
	boolean[] marca = new boolean[grafo.listaDeVertices().size() + 1];
	ListaGenerica<Vertice<T>> currPath = new ListaGenerica<>();
	ListaGenerica<Vertice<T>> recorrido = new ListaGenerica<>();
	this.dfsConCosto(start, grafo, currPath, marca, recorrido, end);
	return recorrido;
    }

    private void dfsConCosto(Vertice<T> vActual,
			     Grafo<T> grafo,
			     ListaGenerica<Vertice<T>> currPath,
			     boolean[] marca,
			     ListaGenerica<Vertice<T>> recorrido,
			     Vertice<T> vDestino)
    {
	// Procesando entrada
	currPath.add(vActual);
	marca[vActual.posicion()] = true;
	if (vActual == vDestino) {
	    recorrido.addAll(currPath);
	    return;
	}
	// Instanciacion
	for (Arista<T> arista : grafo.listaDeAdyacentes(vActual)) {
	    int verPos = arista.verticeDestino().posicion();
	    if (!marca[verPos]) {
		dfsConCosto(arista.verticeDestino(), grafo, currPath, marca, recorrido, vDestino);
	    }
	}
	// Cleanup
	currPath.removeLast();
    }
}
