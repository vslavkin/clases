public class Main {
    public static void main(String[] args) {
	ciudades<String> caminos = new ciudades<>();
	Grafo<String> grafo = new GrafoImpl<>();
	Vertice<String> comienzo = new VerticeImpl<String>("Comienzo", 0);
	Vertice<String> camino1 = new VerticeImpl<String>("Camino1", 1);
	Vertice<String> camino2 = new VerticeImpl<String>("Camino2", 2);
	Vertice<String> camino3 = new VerticeImpl<String>("Camino3", 3);
	Vertice<String> Final = new VerticeImpl<String>("Final", 4);
	grafo.agregarVertice(comienzo);
	grafo.agregarVertice(camino1);
	grafo.agregarVertice(camino2);
	grafo.agregarVertice(camino3);
	grafo.agregarVertice(Final);
	grafo.conectar(comienzo, camino1);
	grafo.conectar(comienzo, camino2);
	grafo.conectar(comienzo, camino3);
	grafo.conectar(camino3, Final);
	grafo.conectar(camino1, camino2);
	grafo.conectar(camino2, comienzo);
	ListaGenerica<Vertice<String>> camino = caminos.dfsConCosto(grafo, comienzo, Final);
	for (Vertice<String> ciudad : camino) {
	    System.out.println(ciudad.dato() + " ↓");
	    }
    }
}
