public class GeneralTreeTest {
    public static void main(String[] args) {
	GeneralTree<Integer> t1 = new GeneralTree<Integer>(1);
	t1.addChild(new GeneralTree<Integer>(3));
	t1.addChild(new GeneralTree<Integer>(8));
	t1.addChild(new GeneralTree<Integer>(10));
	t1.addChild(new GeneralTree<Integer>(5));
	RecorridosAG r1 = new RecorridosAG();
	System.out.println("nImparesM: " + r1.numerosImparesMayoresQuePreOrden(t1, 1));
    }
}
