import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;

public class GeneralTree<T> {
    private T data;
    private List<GeneralTree<T>> children;

    public GeneralTree(T initData) {
	this.data = initData;
	this.children = new LinkedList<GeneralTree<T>>();
    }
    public GeneralTree(T initData, List<GeneralTree<T>> initChildren){
	this.data = initData;
	this.children = initChildren;
    }
    //Data-------------
    public T getData () {
	return this.data;
    }
    public void setData (T newData) {
	this.data = newData;
    }

    //Children-------------
    public List<GeneralTree<T>> getChildren () {
	return this.children;
    }
    public void setChildren (List<GeneralTree<T>> newChildrens) {
	this.children = newChildrens;
    }
    public void addChild (GeneralTree<T> newChild) {
	this.getChildren().add(newChild);
    }
    public void removeChild (GeneralTree<T> childToRemove) {
	this.getChildren().remove(childToRemove);
    }
    //
    public boolean hasChildren () {
	return !(this.getChildren().isEmpty());
    }
    public boolean isEmpty () {
	return this.getChildren().isEmpty();
    }

}
