import java.util.LinkedList;
import java.util.List;

public class RecorridosAG {
    public List<Integer> numerosImparesMayoresQuePreOrden (GeneralTree<Integer> a, Integer n) {
	List<Integer> numeros = new LinkedList<Integer>();
	Integer data = a.getData();
	if (data > n && (data%2 == 1)) {
	    numeros.add(data);
	}
	for (GeneralTree<Integer> child : a.getChildren()) {
	    numeros.addAll(numerosImparesMayoresQuePreOrden(child, n));
	}
	return numeros;
    }
}
