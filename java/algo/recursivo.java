import java.util.ArrayList;
import java.util.LinkedList;

class recursion {
    public static void main(String[] args) {
	System.out.println("Hello world");
	ArrayList<Integer> myList = new ArrayList<>();
	for (int i=0; i<9; i++) {
	    myList.add(i);
	}
	System.out.println(myList);
	flipArr1(myList);
	System.out.println(myList);

	LinkedList<Integer> llist = new LinkedList<Integer>();
	for (int i=0; i<9; i++) {
	    llist.add(i);
	}
	System.out.println("llist" + llist);
	System.out.println("sum:" + sumllist(llist));

	ArrayList<Integer> list1 = new ArrayList<>();
	ArrayList<Integer> list2 = new ArrayList<>();
	list1.add(4);
	list1.add(6);
	list1.add(8);
	list2.add(1);
	list2.add(3);
	list2.add(6);
	System.out.println(combinarOrdenado(list1, list2));
    }
    public static ArrayList<Integer> combinarOrdenado(ArrayList<Integer> lista1, ArrayList<Integer> lista2) {
	ArrayList<Integer> list = new ArrayList<>();
	System.out.println((lista1.size() + lista2.size()));
	for (int i = 0; i < (lista1.size() + lista2.size()); i++) {
	    System.out.println(i);
	    System.out.println("lista1" + lista1.get(0) + " " + lista2.get(0));
	    if (lista1.get(0) < lista2.get(0)) {
			list.add(lista1.get(0));
			lista1.remove(0);
		}
	    else {
			list.add(lista2.get(0));
			lista2.remove(0);
		}
	    }
	System.out.println("lista1" + lista1.get(0) + " " + lista2.get(0));
	return list;
    }
    //NOTA: Los objetos como ArrayList son pasados por referencia, por lo que NO aumenta el stack
    // O(n)
    private static <T> void flipArr1 (ArrayList<T> arr, int start, int end) {
	if (start >= end) {
	    return;
	};
	T tmp;
	tmp = arr.get(start);
	arr.set(start, arr.get(end));
	arr.set(end, tmp);
	flipArr1(arr, start+1, end-1);
    }
    private static void flipArr1 (ArrayList<Integer> arr) {
	flipArr1(arr, 0, arr.size()-1);
    }

    private static int sumllist (LinkedList<Integer> llist, int index) {
	if (index >= llist.size()) {
	    return 0;
	}
	return (llist.get(index) + sumllist(llist, index+1));
    }
    private static int sumllist (LinkedList<Integer> llist) {
	return sumllist(llist, 0);
    }
}
