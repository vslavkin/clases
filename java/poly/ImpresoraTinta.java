public class ImpresoraTinta {
    private String marca;
    private String modelo;
    private CartuchoTinta cartucho;

    public ImpresoraTinta (String marca, String modelo, CartuchoTinta cartucho) {
	this.marca = marca;
	this.modelo = modelo;
	this.cartucho = cartucho;
    }

    public void imprimir (Documento doc){
	cartucho.consumirTinta();
	System.out.println(doc.getFileName() + ": ");
	System.out.println(doc.getFileContents());
    }
}
