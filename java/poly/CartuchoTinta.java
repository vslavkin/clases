public class CartuchoTinta {
    private String modelo;
    private int capacidad;
    private double disponible;

    public CartuchoTinta (String modelo, int capacidad) {
	this.modelo = modelo;
	this.capacidad = capacidad;
    }

    public CartuchoTinta (String modelo, int capacidad, double disponible) {
	this.modelo = modelo;
	this.capacidad = capacidad;
	this.disponible = disponible;
    }

    public double getDisponible() {
	return this.disponible;
    }

    public void consumirTinta() {
	disponible--;
    }
}
