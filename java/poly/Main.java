public class Main {
    public static void main(String[] args) {
	CartuchoTinta cartucho1 = new CartuchoTinta("HP 21", 5);
	ImpresoraTinta impresora1 = new ImpresoraTinta("HP", "F300", cartucho1);
	Documento documento1 = new Documento("ejemplo.doc", "este es un ejemplo de documento");
	impresora1.imprimir(documento1);
    }
}
