public class Documento {
    private String fileName;
    private String fileContents;

    public Documento(String fileName, String fileContents) {
	this.fileName = fileName;
	this.fileContents = fileContents;
    }

    public String getFileName(){
	return this.fileName;
    }
    public String getFileContents(){
	return this.fileContents;
    }
}
