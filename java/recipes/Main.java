import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
	RecipeReviewApp app = new RecipeReviewApp();
	Scanner scanner = new Scanner(System.in);

	// Add recipes
	Recipe recipe1 = new Recipe("Chocolate Cake");
	recipe1.addIngredient("Flour");
	recipe1.addIngredient("Sugar");
	// Add more ingredients and instructions
	app.addRecipe(recipe1);

	// Add reviews
	Review review1 = new Review(recipe1, "Delicious! Easy to follow instructions.", 4.5);
	app.addReview(review1);

	// Display recipes and reviews
	app.displayRecipes();
	app.displayReviews();
    }
}

class RecipeReviewApp {
    private List<Recipe> recipes;
    private List<Review> reviews;

    public RecipeReviewApp() {
	this.recipes = new ArrayList<>();
	this.reviews = new ArrayList<>();
    }

    public void addRecipe(Recipe recipe) {
	recipes.add(recipe);
    }

    public void addReview(Review review) {
	reviews.add(review);
    }

    public void displayRecipes() {
	System.out.println("Available Recipes:");
	for (Recipe recipe : recipes) {
	    System.out.println(recipe.getName());
	    System.out.println("Ingredients:");
	    for (String ingredient : recipe.getIngredients()) {
		System.out.println("- " + ingredient);
	    }
	    System.out.println("Instructions:");
	    for (String instruction : recipe.getInstructions()) {
		System.out.println("- " + instruction);
	    }
	    System.out.println("Rating: " + recipe.getRating());
	    System.out.println();
	}
    }

    public void displayReviews() {
	System.out.println("Recipe Reviews:");
	for (Review review : reviews) {
	    System.out.println("Recipe: " + review.getRecipe().getName());
	    System.out.println("Review: " + review.getReview());
	    System.out.println("Rating: " + review.getRating());
	    System.out.println();
	}
    }
}
