import java.util.Map;
import java.util.Set;

public class Electrodomestico {
    private static final Set<String> coloresDisponibles = Set.of("blanco", "negro", "rojo", "azul", "gris");
    private static final Map<Character, Integer> precioPorConsumo = Map.of('A',100, 'B', 80, 'C',60, 'D',50, 'E',30, 'F',10);
    private static final List<List<Integer>> precioPorPeso = List.of(List.of(20,10), List.of(50,50), List.of(80,80), List.of(Integer.MAX_VALUE, 100));

    private int precioBase = 100;
    private String color = "Blanco";
    private char consumo = 'F';
    private int peso = 5;

    private char comprobarConsumoEnergetico(char consumo) {
	if (consumo >= 'A' && consumo < 'F') {
	    return consumo;
	}
	return 'F';
    }
    private String comprobarColor(String color) {
	if (coloresDisponibles.contains(color.toLowerCase())) {
	    return color.toLowerCase();
	}
	return "blanco";
    }
    private int precioFinal() {
	int precioFinal = this.precioBase;
	for (int i=0; i<precioPorPeso.size(); i++) {
	    if (this.peso < precioPorPeso[i][0]) {
		precioFinal += precioPorPeso[i][1];
	    }
	}
	precioFinal += precioPorConsumo.get(this.consumo);
    }

    public Electrodomestico(int precioBase, int peso){
	this.precioBase = precioBase;
	this.peso = peso;
    }
    public Electrodomestico(int precioBase, String color, String consumo, int peso){
	this.precioBase = precioBase;
	this.color = comprobarColor(color);
	this.consumo = comprobarConsumoEnergetico(consumo);
	this.peso = peso;
    }

    public char getConsumo(char consumo) {
	return this.consumo = consumo;
    }
}
